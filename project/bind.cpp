//bind.cpp

#include "bind.hpp"


void func(const bool b) {
	const_cast<bool&>(b) = !*reinterpret_cast<const char*>(&b);
}

bool func(const bool b, const char c) {
    return b ? (c == '0' + 7) : (c == '7');
}

int func(const bool b, const int i, const FLAGS f) {
    int r;

    if (b) r = i / static_cast<int>(f);
    else r = 28;

    r = r - 27;

    return r;
}