#pragma once //bind.hpp

#include <functional> // std::bind
using namespace std::placeholders;

enum FLAGS { F1, F2, F3 };

void func(bool b);
bool func(bool b, char c);
int  func(bool b, int i, FLAGS f = F3);


//---------------!!! From here you may change this file !!!------------------

// Main task (2 points):
/*\
 *  Bind certain (you have to figure out - which) 'func' overload
 *
 *  See "bind.cpp" and "bind_gTests.cpp" : Bind part
\*/

static auto bind = std::bind(



// Alternate solution (1 point)
/*\
 *  There is also a way to "outcheat" this task:

 *  Make: static "cheat" <function / class object / whatever else>
 *  that also solves this task.

 *  See "bind_gTests.cpp" : Cheat part
\*/


static bool cheat(bool b, char c) { return func(b, c); } // delete this



// a Glass of Lore
/*\
 * Situation:
 * 
 * You want to bind a function.
 * But it has several overloads.
 *
 * So what to do?
\*/