//bind_gTest.cpp
/*
 * If you are not familiar with Google Test, please follow the link:
 * https://github.com/google/googletest/blob/master/googletest/docs/primer.md
 *
 */

#include "bind.hpp"

#include "gtest/gtest.h"

#include <iostream>
#include <functional> //std::is_bind_expression

static int points = 0;


//======================= Bind tests part =========================

TEST(Bind, is_bind)
{
    ASSERT_TRUE(std::is_bind_expression<decltype(bind)>::value);
    points += 2;
}

TEST(Bind, check)
{
    ASSERT_EQ(0, bind(true, '7'));
    points += 2;
}


//---------------------- Cheat tests part ---------------------------

TEST(Cheat, is_bind)
{
    ASSERT_TRUE(std::is_bind_expression<decltype(cheat)>::value);
    points++;
}

TEST(Cheat, check)
{
    ASSERT_EQ(0, cheat(true, '7'));
    points++;
}


int main(int argc, char** argv)
{
    int stat;
    
    static_assert(
        FLAGS::F1 == 0 &&
        FLAGS::F2 == 1 &&
        FLAGS::F3 == 2,
        "unauthorized changes in bind.h: 'enum FLAGS {...};' detected!"
        );

    std::cout << "[ Running  ] main(..) from bind_gTest.cpp...\n";

    testing::InitGoogleTest(&argc, argv);
    stat = RUN_ALL_TESTS();

    if (points == 2 || points >= 4) std::cout << "[! PASSED !] Congratulations! :)\n";
    std::cout << "[  Result  ] At maximum " << points / 2 << " points earned\n";

    return stat;
}
