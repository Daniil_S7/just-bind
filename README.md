## Just Bind

Just Bind - is a test for _searching skill_ and/or _C++ capabilities knowledge_

----

#### Main task (2 points):
Bind certain (you have to figure out - which) 'func' overload
See `bind.cpp` and `bind_gTests.cpp` : Bind part


#### Alternate solution (1 point)
There is also a way to "outcheat" this task:

Make: static "cheat" <function / class object / whatever else> that also solves this task.
See `bind_gTests.cpp` : Cheat part

> The list of files that are allowed to be modified:
> -    `project/bind.hpp`

----

> Attention! **Google Tests** must be installed inside a higher directory (`../googletest`).

>**Difficulty gradation**:
> - **Trainee/Junior** - just solve _by any means_ (approximate: up to 2 hours)
> - **Middle** - solve _by any means_ (approximate: up to 30 mins)
> - **Senior** - solve without _internet_ (approximate: up to 10 mins)

<!--

-->
